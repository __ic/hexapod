# Hexapod

Convenient access to sample code for playing with HiWonder's SpiderBot for RaspberryPi.

The sample code specifies no copyright, yet it is provided openly by HiWonder on their bots and [website](https://www.hiwonder.hk/) (thank you, by the way).

One piece of code does specify a copyright: `Mpu6050.py`, with note `Copyright (c) 2015, 2016, 2017 MrTijn/Tijndagamer`.
